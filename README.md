# Deploy PGAdmin & WebApp for administration of Odoo & PostgreSQL

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# 1. Ansible Playbook

````bash
mkdir /path/pgadmin-project
````

````bash
cd /path/pgadmin-project
````

````bash
nano pgadmin-deploy.yml
````

## Content
````yaml
---
- name: Setup PGAdmin
  hosts: localhost
  vars_files:
    - group_vars/all.yml
  roles:
    - pgadmin_deploy
````

# 2. Requirement

````bash
mkdir /path/pgadmin-project/roles
````

````bash
nano /path/pgadmin-project/roles/requirements.yml
````

````yaml
- src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/pgadmin_deploy.git
  scm: git
  version: "main"
  name: pgadmin_deploy
````


# 3. Ajust variables

````bash
mkdir /path/pgadmin-project/group_vars
````

````bash
nano /path/pgadmin-project/group_vars/all.yml
````

| Variable               | Valeur par défaut                | Description                                             |
|------------------------|----------------------------------|---------------------------------------------------------|
| `pgadmin_compose_directory`| `.`                         | Répertoire contenant le fichier docker-compose pour PGAdmin |
| `pgadmin_compose_file`     | `pgadmin-template.yml.j2`    | fichier template docker-compose pour PGAdmin          |
| `PGADMIN_PASSWORD`         | `admin`                      | Mot de passe par défaut pour PGAdmin                  |
| `PGADMIN_IMAGE`            | `dpage/pgadmin4`             | Image Docker Officiel pour PGAdmin                    |
| `WEBAPP_IMAGE`             | `carlfg/ic-webapp:latest`    | Image Docker pour l'application web IC-WebApp         |
| `PGADMIN_EMAIL`            | `fongangcarlin@gmail.com`    | Email utilisé pour se connecter à PGAdmin             |
| `PGADMIN_NETWORK`          | `pgadmin_network`            | Réseau Docker utilisé pour PGAdmin                    |
| `PGADMIN_EXPOSE_PORT`      | `5060`                       | Port sur lequel PGAdmin est exposé                    |
| `WEBAPP_EXPOSE_PORT`       | `8095`                       | Port sur lequel IC-WebApp est exposé                  |
| `PGADMIN_VOLUME`           | `pgadmin-data`               | Volume Docker pour la persistance des données PGAdmin |
| `PGADMIN_SERVICE_NAME`     | `pgadmin`                    | Nom du service Docker pour PGAdmin                    |
| `WEBAPP_SERVICE_NAME`      | `ic-webapp`                  | Nom du service Docker pour IC-WebApp                  |



# 4. Launch

````bash
cd /path/pgadmin-project
````

````bash
ansible-galaxy install -r roles/requirements.yml -f
````

````bash
ansible-playbook pgadmin-deploy.yml -vvv
````

🥳🎉🥳 : Félicitation, Odoo est déployé sur votre serveur


